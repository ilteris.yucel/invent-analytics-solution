import { Sequelize } from "sequelize";
import { Model, DataTypes } from "sequelize";

const sequelize = new Sequelize('postgres://ilteris:ilteris@localhost:5432/library')

class User extends Model {}
User.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING,
    nonNull: true,
    unique: true
  }
}, {
  modelName: "library",
  tableName: 'users',
  sequelize,
});


class Book extends Model {}
Book.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.STRING,
    nonNull: true,
    unique: true
  }
}, {
  modelName: "library",
  tableName: 'books',
  sequelize,
});



class Borrow extends Model {}
Borrow.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  userScore: {
    type: DataTypes.INTEGER,
  },
  returned: {
    type: DataTypes.BOOLEAN
  }
}, {
  modelName: "library",
  tableName: 'borrows',
  sequelize,
});


User.hasMany(Borrow, {
  foreignKey: 'userId'
});
Borrow.belongsTo(User);

Book.hasMany(Borrow, {
  foreignKey: "bookId"
});
Borrow.belongsTo(Book);

User.sync({ alter: true });
Book.sync({ alter: true });
Borrow.sync({ alter: true });

export { sequelize, User, Book, Borrow };

