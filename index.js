import express from 'express';
import bodyParser from 'body-parser';
import { sequelize } from './db/index.js';
import usersRouter from './users/index.js';
import booksRouter from './books/index.js';

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', (req, res) => {
  res.send('Successful response.');
});

app.use('/users', usersRouter);
app.use('/books', booksRouter);

app.listen(3000, async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
  console.log('Application is successfully created.');
});