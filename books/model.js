import { Book } from "../db/index.js";



class BooksModel {
  async addBook(book){
    try {
      await Book.create(book);
      return book.name;
    } catch (e) {
      throw e;
    }
  }
  async getBook(bookId){
    try {
      const book = await Book.findByPk(bookId);
      return book;
    } catch (e) {
      throw e;
    }
  }
  async getBooks(){
    try {
      const books = await Book.findAll();
      return books;
    } catch (e) {
      throw e;
    }
  }
}

export default BooksModel