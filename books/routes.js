import express from 'express';
import BooksService from './service.js';

const booksRouter = express.Router();
const service = new BooksService();

booksRouter.post('/', async (req, res, next) => {
  return await service.addBook(req, res, next);
});

booksRouter.get('/:id', async (req, res, next) => {
  return await service.getBook(req, res, next);
});

booksRouter.get('/', async (req, res, next) => {
  return await service.getBooks(req, res, next);
});

export default booksRouter;
