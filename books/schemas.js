import Joi from "joi";

const addBookSchema = Joi.object({
  name: Joi.string().min(8).max(256),
});

const getBookSchema = Joi.string().pattern(/^[0-9]+$/).required();


export {
  addBookSchema,
  getBookSchema,
};