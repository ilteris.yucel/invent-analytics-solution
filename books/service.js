import { addBookSchema, getBookSchema } from "./schemas.js";
import BooksModel from "./model.js";

const booksModel = new BooksModel();

class BooksService {
  async addBook(req, res, next) {
    try{
      const { body } = req;
      const {error, value} = addBookSchema.validate(body);
      if(error) {
        return res.status(400).send(error.details.message);
      }
      const result = await booksModel.addBook(value); 
      return res.status(201).json({name: result});
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
  async getBook(req, res, next) {
    try{
      const { id } = req.params;
      const {error, value} = getBookSchema.validate(id);
      if(error) {
        return res.status(400).send(error.details.message);
      }
      const result = await booksModel.getBook(value); 
      return res.status(200).json(result);
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
  async getBooks(req, res, next) {
    try{
      const result = await booksModel.getBooks(); 
      return res.status(200).json(result);
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
}

export default BooksService;