import Joi from "joi";

const addUserSchema = Joi.object({
  name: Joi.string().min(8).max(256),
});

const getUserSchema = Joi.string().pattern(/^[0-9]+$/).required();


export {
  addUserSchema,
  getUserSchema,
};