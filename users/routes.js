import express from 'express';
import UsersService from './service.js';

const usersRouter = express.Router();
const service = new UsersService();

usersRouter.post('/', async (req, res, next) => {
  return await service.addUser(req, res, next);
});

usersRouter.get('/:id', async (req, res, next) => {
  return await service.getUser(req, res, next);
});

usersRouter.get('/', async (req, res, next) => {
  return await service.getUsers(req, res, next);
});

export default usersRouter;
