import { User } from "../db/index.js";



class UsersModel {
  async addUser(user){
    try {
      await User.create(user);
      return user.name;
    } catch (e) {
      throw e;
    }
  }
  async getUser(userId){
    try {
      const user = await User.findByPk(userId);
      return user;
    } catch (e) {
      throw e;
    }
  }
  async getUsers(){
    try {
      const users = await User.findAll();
      return users;
    } catch (e) {
      throw e;
    }
  }
}

export default UsersModel