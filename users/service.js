import { addUserSchema, getUserSchema } from "./schemas.js";
import UsersModel from "./model.js";

const usersModel = new UsersModel();

class UsersService {
  async addUser(req, res, next) {
    try{
      const { body } = req;
      const {error, value} = addUserSchema.validate(body);
      if(error) {
        return res.status(400).send(error.details.message);
      }
      const result = await usersModel.addUser(value); 
      return res.status(201).json({name: result});
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
  async getUser(req, res, next) {
    try{
      const { id } = req.params;
      const {error, value} = getUserSchema.validate(id);
      if(error) {
        return res.status(400).send(error.details.message);
      }
      const result = await usersModel.getUser(value); 
      return res.status(200).json(result);
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
  async getUsers(req, res, next) {
    try{
      const result = await usersModel.getUsers(); 
      return res.status(200).json(result);
    } catch(e){
      return res.status(500).send("Internal Server Error");
    }

  }
}

export default UsersService;